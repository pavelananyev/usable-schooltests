from django.apps import AppConfig


class SchooltestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'schooltest'
